#include <cstdio>
#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

const int MAXV = 105;
const int MAXE = 10050;

struct edge{
    int u, v;
    double w;
} g[MAXE];

double x[MAXV], y[MAXV], z[MAXV], r[MAXV];
int f[MAXV];
int n, top;

int father(int x) {
    if (x == f[x]) return x;
    f[x] = father(f[x]);
    return f[x];
}

void addedge(int u, int v, double w) {
    top++; g[top].u = u, g[top].v = v, g[top].w = w;
}

bool comp(edge x, edge y) {
    if (x.w < y.w) return true; else return false;
}

double d2(double x1, double y1, double z1, double x2, double y2, double z2) {
    return pow(x1 - x2, 2) + pow(y1 - y2, 2) + pow(z1 - z2, 2);
}

int main()
{
    while (scanf("%d", &n) != EOF)
    {
        if (n == 0) break; top = 0;
        for (int i=1 ; i<=n ; i++) {
            scanf("%lf%lf%lf%lf", &x[i], &y[i], &z[i], &r[i]);
            f[i] = i;
        }
        for (int i=1 ; i<=n ; i++) {
            for (int j=1 ; j<=i-1 ; j++) {
                if (d2(x[i], y[i], z[i], x[j], y[j], z[j]) <= pow(r[i] + r[j], 2))
                    f[father(i)] = father(j);
                else addedge(i, j, sqrt(d2(x[i], y[i], z[i], x[j], y[j], z[j])) - r[i] - r[j]);
            }
        }
        sort(g + 1, g + top + 1, comp);

        double res = 0.0;
        for (int i=1 ; i<=top ; i++) {
            if (father(g[i].u) != father(g[i].v)) {
                f[father(g[i].u)] = father(g[i].v);
                res += g[i].w;
            } else continue;
        }
        printf("%.3f\n", res);
    }
    return 0;
}