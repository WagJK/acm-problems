#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

const int MAXV = 30;
const int MAXE = 100;

struct edge{
    int u, v, w;
} g[MAXE];

int f[MAXV];
int n, m, top;

int father(int x) {
    if (x == f[x]) return x;
    f[x] = father(f[x]);
    return f[x];
}

void addedge(int u, int v, int w) {
    top++; g[top].u = u, g[top].v = v, g[top].w = w;
}

bool comp(edge x, edge y) {
    if (x.w < y.w) return true; else return false;
}

int main()
{
    char u[5], v[5]; int w, res = 0;
    while (scanf("%d", &n) != EOF) {
        if (n == 0) break;
        top = 0, res = 0;
        for (int i=1 ; i<=n ; i++)
            f[i] = i;
        for (int i=1 ; i<=n-1 ; i++) {
            scanf("%s", &u);
            scanf("%d", &m);
            for (int j=1 ; j<=m ; j++) {
                scanf("%s", &v);
                scanf("%d", &w);
                addedge(u[0] - 'A' + 1, v[0] - 'A' + 1, w);
            }
        }
        sort(g + 1, g + top + 1, comp);
        for (int i=1 ; i<=top ; i++) {
            if (father(g[i].u) != father(g[i].v)) {
                f[father(g[i].u)] = father(g[i].v);
                res += g[i].w;
            } else continue;
        }
        printf("%d\n", res);
    }
    return 0;
}