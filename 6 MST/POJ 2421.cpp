#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

const int MAXV = 105;
const int MAXE = 10050;

struct edge{
    int u, v, w;
} g[MAXE];

int f[MAXV];
int n, m, top;

int father(int x) {
    if (x == f[x]) return x;
    f[x] = father(f[x]);
    return f[x];
}
void addedge(int u, int v, int w) {
    top++; g[top].u = u, g[top].v = v, g[top].w = w;
}
bool comp(edge x, edge y) {
    if (x.w < y.w) return true; else return false;
}

int main()
{
    int u, v, w, q, res = 0;
    scanf("%d", &n);
    top = 0, res = 0;
    for (int i=1 ; i<=n ; i++)
        f[i] = i;
    for (int i=1 ; i<=n ; i++) {
        for (int j=1 ; j<=n ; j++){
            scanf("%d", &w);
            if (i < j) addedge(i, j, w);
        }
    }
    scanf("%d", &q);
    for (int i=1 ; i<=q ; i++) {
        scanf("%d%d", &u, &v);
        f[father(u)] = father(v);
    }
    sort(g + 1, g + top + 1, comp);
    for (int i=1 ; i<=top ; i++) {
        if (father(g[i].u) != father(g[i].v)) {
            f[father(g[i].u)] = father(g[i].v);
            res += g[i].w;
        } else continue;
    }
    printf("%d\n", res);
    return 0;
}