#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

const int MAXV = 60;
const int MAXE = 5000;

struct edge{
    int u, v, w;
} g[MAXE];

int f[MAXV];
int n, m, top;

int father(int x) {
    if (x == f[x]) return x;
    f[x] = father(f[x]);
    return f[x];
}

void addedge(int u, int v, int w) {
    top++; g[top].u = u, g[top].v = v, g[top].w = w;
}

bool comp(edge x, edge y) {
    if (x.w < y.w) return true; else return false;
}

int main()
{
    int u, v, w, res = 0;
    while (scanf("%d", &n) != EOF) {
        if (n == 0) break;
        top = 0, res = 0;
        for (int i=1 ; i<=n ; i++)
            f[i] = i;
        scanf("%d", &m);
        for (int i=1 ; i<=m ; i++) {
            scanf("%d%d%d", &u, &v, &w);
            addedge(u, v, w);
        }
        sort(g + 1, g + top + 1, comp);
        for (int i=1 ; i<=top ; i++) {
            if (father(g[i].u) != father(g[i].v)) {
                f[father(g[i].u)] = father(g[i].v);
                res += g[i].w;
            } else continue;
        }
        printf("%d\n", res);
    }
    return 0;
}