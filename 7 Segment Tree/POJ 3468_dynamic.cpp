#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <cstdlib>
using namespace std;
const int MAXL = 5000000;
long long a,n,q,l,r,m;
char ch;

struct SegTree{
	int top;
	int ll[MAXL], rr[MAXL];
	int lson[MAXL], rson[MAXL];
	long long lazy[MAXL], sum[MAXL];

	SegTree(int l, int r){
		top = 1; ll[1] = l; rr[1] = r;
		for (int i=1 ; i<MAXL ; i++)
            sum[i] = lazy[i] = lson[i] = rson[i] = 0;
	}
	void download(int x){
		// should be altered here
		build(x);
		sum[x] += (rr[x] - ll[x] + 1) * lazy[x];
		lazy[lson[x]] += lazy[x];
		lazy[rson[x]] += lazy[x];
		lazy[x] = 0;
	}
	void build(int x){
	    if (ll[x]==rr[x] || x==0) return;
		int mid = (ll[x]+rr[x])/2;
		if (lson[x]==0){
			lson[x] = ++top;
			ll[lson[x]] = ll[x];
			rr[lson[x]] = mid;
		}
		if (rson[x]==0){
			rson[x] = ++top;
			ll[rson[x]] = mid+1;
			rr[rson[x]] = rr[x];
		}
	}
	void insert(int x, int l, int r, int m){
		build(x);
		if (lazy[x]!=0) download(x);
		if (ll[x]==l && rr[x]==r)
			{lazy[x] += m; download(x); return;}
		int mid = (ll[x] + rr[x]) / 2;
		if (r<=mid) {insert(lson[x],l,r,m); download(rson[x]);}
		if (l>mid) 	{insert(rson[x],l,r,m); download(lson[x]);}
		if (l<=mid && r>mid){
			insert(lson[x], l, mid, m);
			insert(rson[x], mid+1, r, m);
		}
		// should be altered here
		sum[x] = sum[lson[x]] + sum[rson[x]];
	}
	long long query(int x, int l, int r){
		build(x);
		if (lazy[x]!=0) download(x);
		if (ll[x]==l && rr[x]==r) return sum[x];
		int mid = (ll[x] + rr[x]) / 2;
		if (r<=mid) return query(lson[x], l, r);
		if (l>mid)  return query(rson[x], l, r);
		return query(lson[x], l, mid) + query(rson[x], mid+1, r);
	}
};
int main(){
	cin >> n >> q;
	SegTree *T = new SegTree(1,n);
	
	for (int i=1;i<=n;i++){
		scanf("%lld",&a);
		T->insert(1,i,i,a);
	}
	
    scanf("%c",&ch);
	for (int i=1;i<=q;i++){
		scanf("%c",&ch);
		if (ch=='C'){
			scanf("%lld%lld%lld",&l,&r,&m);
			T->insert(1,l,r,m);
		}
		else{
			scanf("%lld%lld",&l,&r);
			printf("%lld\n",T->query(1,l,r));
		}
		scanf("%c",&ch);
	}
}