#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <cstdlib>
using namespace std;
const int MAXL = 2000000;
int a[800000],n,q,l,r,m;
char ch;

struct SegTree{
	int ll[MAXL], rr[MAXL];
	long long sum[MAXL], lazy[MAXL];

	void download(int x){
		// should be altered here
		sum[x] += (rr[x] - ll[x] + 1) * lazy[x];
		lazy[2*x] += lazy[x];
		lazy[2*x+1] += lazy[x];
		lazy[x] = 0;
	}
	void build(int *a, int x, int l, int r){
		ll[x] = l;
		rr[x] = r;
		int mid = (l + r) / 2;
		if (l==r) {sum[x] = a[l]; return;}
		build(a, 2*x, l, mid);
		build(a, 2*x+1, mid+1, r);
		// should be altered here
		sum[x] = sum[2*x] + sum[2*x+1];
	}
	void insert(int x, int l, int r, int m){
		if (lazy[x]!=0) download(x);
		if (ll[x]==l && rr[x]==r)
			{lazy[x] += m; download(x); return;}
		int mid = (ll[x] + rr[x]) / 2;
		if (r<=mid) {insert(2*x,l,r,m); download(2*x+1);}
		if (l>mid) 	{insert(2*x+1,l,r,m); download(2*x);}
		if (l<=mid && r>mid){
			insert(2*x, l, mid, m);
			insert(2*x+1, mid+1, r, m);
		}
		// should be altered here
		sum[x] = sum[2*x] + sum[2*x+1];
	}
	long long query(int x, int l, int r){
		if (lazy[x]!=0) download(x);
		if (ll[x]==l && rr[x]==r) return sum[x];
		int mid = (ll[x] + rr[x]) / 2;
		if (r<=mid) return query(2*x, l, r);
		if (l>mid)  return query(2*x+1, l, r);
		return query(2*x, l, mid) + query(2*x+1, mid+1, r);
	}
};
int main(){
	cin >> n >> q;
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	SegTree *T = new SegTree();
	T->build(a,1,1,n);

    scanf("%c",&ch);
	for (int i=1;i<=q;i++){
		scanf("%c",&ch);
		if (ch=='C'){
			scanf("%d%d%d",&l,&r,&m);
			T->insert(1,l,r,m);
		}
		else{
			scanf("%d%d",&l,&r);
			printf("%lld\n",T->query(1,l,r));
		}
		scanf("%c",&ch);
	}
}