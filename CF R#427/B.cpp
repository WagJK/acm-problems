#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <queue>

using namespace std;

int k, res, sum = 0;
char n[200005];

int main()
{
	scanf("%d\n", &k);
	scanf("%s", n);

	for (int i=0 ; i<strlen(n) ; i++)
		sum += n[i] - '0';
	if (sum >= k) {
		printf("0\n");
	} else {
		int temp = k - sum;
		sort(n, n+strlen(n));
		for (int i=0 ; i<strlen(n) ; i++) {
			temp -= '9' - n[i];
			if (temp <= 0) {
				res = i + 1; break;
			}
		}
		printf("%d\n" , res);
	}

	return 0;
}
