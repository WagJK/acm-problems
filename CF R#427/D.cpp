#include <cstdio>
#include <cstring>

const int MAXLEN = 5005;
const int MAXK = 15;

char s[MAXLEN];
bool f[MAXLEN][MAXLEN][3];
bool ff[MAXLEN][MAXLEN];
int kk[MAXK], pow2[MAXK];

int min(int x, int y) {
	if (x < y) return x; else return y;
}
int main()
{
	scanf("%s", s);

	memset(f, false, sizeof(f));
	for (int i=0 ; i<strlen(s) ; i++) {
		for (int j=0 ; j<=min(i, strlen(s)-i-1) ; j++) {
			if (s[i-j] == s[i+j]) {
				f[i-j][i+j][1] = true;
				kk[1]++;
			}
			else break;
		}
		for (int j=0 ; j<=min(i, strlen(s)-i-2) ; j++) {
			if (s[i-j] == s[i+j+1]) {
				f[i-j][i+j+1][1] = true;
				kk[1]++;
			}
			else break;
		}
	}

	int len;
	memset(ff, false, sizeof(ff));
	for (int l=1 ; l<=strlen(s) / 2 ; l++) {
		len = 0;
		for (int i=0 ; i<strlen(s) - l ; i++) {
			if (s[i] == s[i + l]) len++;
			else len = 0;
			if (len >= l) ff[i-l+1][i+l] = true;
		}
		len = 0;
		for (int i=0 ; i<strlen(s) - l - 1 ; i++) {
			if (s[i] == s[i + l + 1]) len++;
			else len = 0;
			if (len >= l) ff[i-l+1][i+l+1] = true;
		}
	}
	/*
	for (int i=0 ; i<strlen(s) ; i++) {
		for (int j=0 ; j<strlen(s) ; j++) {
			if (ff[i][j]) printf("T ");
			else printf("F ");
		}
		printf("\n");
	}
	*/

	pow2[0] = 1;
	for (int i=1 ; i<MAXK ; i++)
		pow2[i] = pow2[i-1] << 1;

	for (int k=2 ; k<MAXK ; k++) {
		for (int i=0 ; i<strlen(s) ; i++) {
			for (int j=i+pow2[k-1]-1 ; j<strlen(s) ; j++) {
				len = (j - i + 1) / 2;
				f[i][j][2] = ff[i][j] && (f[i][i+(len-1)][1] || f[j-(len-1)][j][1]);
				if (f[i][j][2]) kk[k]++;
				// if (f[i][j][k]) printf("ijk %d %d %d\n", i, j, k);
			}
		}
		for (int i=0 ; i<strlen(s) ; i++)
			for (int j=0 ; j<strlen(s) ; j++)
				f[i][j][1] = f[i][j][2];
	}
	for (int i=1 ; i<=strlen(s) ; i++) {
		if (i<MAXK) printf("%d ", kk[i]);
		else printf("0 ");
	}
	printf("\n");
	return 0;
}
