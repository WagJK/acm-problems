#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <queue>

using namespace std;

const int MAXL = 120;
const int MAXC = 15;

int n, q, c, x, y, s;
int t, x1, x2, y1, y2;

int f[MAXL][MAXL][MAXC];
int map[MAXL][MAXL][MAXC];

int query(int t, int x1, int y1, int x2, int y2)
{
	return f[x2][y2][t % c]
		- f[x1-1][y2][t % c]
		- f[x2][y1-1][t % c]
		+ f[x1-1][y1-1][t % c];
}

int main()
{
	scanf("%d%d%d", &n, &q, &c); c++;
	memset(f, 0, sizeof(f));
	memset(map, 0, sizeof(map));
	for (int i=1 ; i<=n ; i++) {
		scanf("%d%d%d", &x, &y, &s);
		for (int j=0 ; j<c ; j++)
			map[x][y][j] += (s + j) % c;
	}
	for (int k=0 ; k<c ; k++) {
		for (int i=1 ; i<MAXL ; i++) {
			for (int j=1 ; j<MAXL ; j++) {
				f[i][j][k] = f[i][j-1][k]
							+ f[i-1][j][k]
							- f[i-1][j-1][k] + map[i][j][k];
			}
		}
	}
	for (int i=1; i<=q ; i++) {
		scanf("%d%d%d%d%d", &t, &x1, &y1, &x2, &y2);
		printf("%d\n", query(t, x1, y1, x2, y2));
	}
	return 0;
}
