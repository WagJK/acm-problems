#include <cstdio>
#include <cstring>

int main()
{
	char s[100], e[100];
	int duration, ss, tt;

	scanf("%s %s", s, e);
	scanf("%d", &duration);
	if (s[0] == 'v') ss = 0;
	if (s[0] == '<') ss = 1;
	if (s[0] == '^') ss = 2;
	if (s[0] == '>') ss = 3;

	if (e[0] == 'v') tt = 0;
	if (e[0] == '<') tt = 1;
	if (e[0] == '^') tt = 2;
	if (e[0] == '>') tt = 3;
	duration = duration % 4;

	int delta = tt - ss;
	if (delta < 0) delta += 4;

	if (duration == delta) {
		if (duration + delta == 4 || duration == 0) printf("undefined\n");
		else printf("cw\n");
	} else {
		if (duration + delta == 4) printf("ccw\n");
		else printf("undefined\n");
	}

	return 0;
}
