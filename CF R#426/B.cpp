#include <cstdio>
#include <iostream>
using namespace std;

char s[1000005];
int f_occ[100], l_occ[100];
int n, k;

int main()
{
	scanf("%d%d", &n, &k);
	scanf("%s", s);

	for (int i=1 ; i<=26 ; i++) {
		f_occ[i] = -1;
		l_occ[i] = -1;
	}

	for (int i=0 ; i<n ; i++)
		l_occ[s[i] - 'A' + 1] = i;
	for (int i=n-1 ; i>=0 ; i--)
		f_occ[s[i] - 'A' + 1] = i;

	int od; bool flag = false;
	for (int i=0 ; i<n ; i++) {
		od = 0;
		for (int j=1 ; j<=26 ; j++) {
			if (f_occ[j] == -1) continue;
			if (i >= f_occ[j] && i <= l_occ[j]) od++;
		}
		if (od > k) flag = true;
	}
	if (flag) printf("YES\n");
	else printf("NO\n");
	return 0;
}
