#include <cstdio>
#include <cstring>

int n;
long long a, b;

int main() {
	scanf("%d", &n);
	bool flag;
	for (int i=1 ; i<=n ; i++) {
		flag = true;
		scanf("%I64d%I64d", &a, &b);

		long long ll = 1, rr = 1000000, mid, k = -1;
		while (ll <= rr) {
			mid = (ll + rr) / 2;
			if (mid * mid * mid == a * b) {
				k = mid; break;
			} else {
				if (mid * mid * mid < a * b) ll = mid + 1;
				else rr = mid - 1;
			}
		}
		
		if (k == -1) flag = false;
		if (a % k != 0) flag = false;
		if (b % k != 0) flag = false;

		if (flag) printf("Yes\n");
		else printf("No\n");
	}
}
