#include <cstdio>
#include <iostream>
#include <cstring>

using namespace std;

const int MAXLEN = 200086;

char good[MAXLEN], s[MAXLEN], q[MAXLEN];
bool f[MAXLEN];
int T;


int main()
{
    scanf("%s", &good);
    for (int i=1 ; i<=26 ; i++)
        f[i] = false;
    for (int i=0 ; i<strlen(good) ; i++)
        f[good[i] - 'a' + 1] = true;

    scanf("%s", &s);
    scanf("%d", &T);

    bool flag = true;
    int pos = -1;

    for (int t=1 ; t<=T ; t++) {
        flag = true;
        pos = -1;

        scanf("%s", &q);
        for (int i=0 ; i<strlen(s) ; i++) {
            if (s[i] == '*'){
                pos = i; break;
            }
            if (s[i] == '?'){
                if (f[q[i] - 'a' + 1]) continue;
                else { flag = false; break; }
            } else if (s[i] != q[i]) {
                flag = false; break;
            }
        }
        
        
        for (int i=0 ; i<strlen(s) ; i++) {
            if (s[strlen(s) - i - 1] == '*') break;
            if (strlen(q) - i - 1 < 0) {
                flag = false; break;
            }
            if (s[strlen(s) - i - 1] == '?'){
                if (f[q[strlen(q) - i - 1] - 'a' + 1]) continue;
                else { flag = false; break; }
            } else if (s[strlen(s) - i - 1] != q[strlen(q) - i - 1]) {
                flag = false; break;
            }
        }

        if (pos != -1) {
            int end = strlen(q) - strlen(s) + pos + 1;
            for (int j=pos ; j<end ; j++) {
                if (f[q[j] - 'a' + 1]) flag = false;
            }
            if (flag) printf("YES\n");
            else printf("NO\n");
        } else {
            if (strlen(s) == strlen(q) && flag) printf("YES\n");
            else printf("NO\n");
        }
        

    }
    
    return 0;
}