#include <cstdio>
#include <iostream>
using namespace std;
const int MAXN = 200086;

struct edge{ int v, w, next; };

edge g[MAXN];
int head[MAXN], d[MAXN], p[MAXN];
int f[MAXN][20], w[MAXN][20];
int n, q, top = 0;

void addedge(int u, int v, int w){
	g[++top].v = v; g[top].w = w; g[top].next = head[u]; head[u] = top;
	g[++top].v = u; g[top].w = w; g[top].next = head[v]; head[v] = top;
}

void dfs(int x, int prev, int depth){
	int temp;
	d[x] = depth;
	f[x][0] = prev;
	temp = head[x];
	while (temp != 0){
		if (g[temp].v != prev){
			w[g[temp].v][0] = g[temp].w;
			dfs(g[temp].v, x, depth+1);
		}
		temp = g[temp].next;
	}	
}
void pre(){
	dfs(1, 0, 1);
	for (int j=1 ; j<=16 ; j++){
		for (int i=1 ; i<=n ; i++){
			f[i][j] = f[f[i][j-1]][j-1];
			w[i][j] = w[i][j-1] + w[f[i][j-1]][j-1];
		}
	}
}

int dis(int x, int y){
	if (x == y) return 0;
	if (d[x] != d[y]){
		if (d[x] < d[y]) return dis(y, x);
		for (int i=1 ; i<=16 ; i++){
			if (d[x] - (1<<i) < d[y])
				return dis(f[x][i-1], y) + w[x][i-1];
		}
	}
	for (int i=1 ; i<=16 ; i++){
		if (f[x][i] == f[y][i] || f[x][i] == 0)
			return dis(f[x][i-1], f[y][i-1]) + w[x][i-1] + w[y][i-1];
	}
}

int max(int x, int y){
    if (x > y) return x; else return y;
}

int lca(int x, int y){
    if (x == y) return x;
	if (d[x] != d[y]){
		if (d[x] < d[y]) return lca(y, x);
		for (int i=1 ; i<=16 ; i++){
			if (d[x] - (1<<i) < d[y])
				return lca(f[x][i-1], y);
		}
	}
	for (int i=1 ; i<=16 ; i++){
		if (f[x][i] == f[y][i] || f[x][i] == 0)
			return lca(f[x][i-1], f[y][i-1]);
	}
}

int main()
{
    scanf("%d%d", &n, &q);
    for (int i=1 ; i<=n-1 ; i++) {
        scanf("%d", &p[i]);
        addedge(i+1, p[i], 1);
    }
    pre();
    int a, b, c;
    for (int i=1 ; i<=q ; i++) {
        scanf("%d%d%d", &a, &b, &c);
        int res = 0;
        // printf("lca:%d %d %d\n", lca(a,b), lca(b,c), lca(c,a));
        // printf("dis:%d %d %d\n", dis(a,b), dis(b,c), dis(c,a));

        res = max(res, dis(c, a) + dis(b, a) - dis(b, c));
        res = max(res, dis(a, b) + dis(c, b) - dis(a, c));
        res = max(res, dis(a, c) + dis(b, c) - dis(a, b));

        printf("%d\n", res / 2 + 1);
    }
    return 0;
}