#include <cstdio>
#include <iostream>

using namespace std;

int main()
{
    long long n, k;
    cin >> n >> k;
    if (((n / k) & 1) == 0) printf("NO\n");
    else printf("YES\n");
    return 0;
}