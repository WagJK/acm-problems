#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>

using namespace std;

const int MAXE = 2400000;
const int MAXV = 400000;
const long long INF = 0x3f3f3f3f3f3f3f3f;

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV];

long long dist[MAXV];
int viscnt[MAXV];
bool inqueue[MAXV];

int n, top = 1;

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

bool spfa(int s) {
    queue<int> q;
    memset(dist, 	0x3f, 	sizeof(dist));
    memset(viscnt,  0,      sizeof(viscnt));
	memset(inqueue, false, 	sizeof(inqueue));

	dist[s] = 0;
    q.push(s); inqueue[s] = true;
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
                if (!inqueue[g[i].v]) {
                    q.push(g[i].v); inqueue[g[i].v] = true;
                    viscnt[g[i].v]++;
                    if (viscnt[g[i].v] > n-1) return false;
                }
			}
		}
        inqueue[u] = false;
	}
    return true;
}

int main()
{
    int a, b, d, ml, md;
    scanf("%d%d%d", &n, &ml, &md);
    for (int i=1 ; i<=n-1 ; i++) {
        addedge(i+1, i, 0);
    }
    for (int i=1 ; i<=ml ; i++) {
        scanf("%d%d%d", &a, &b, &d);
        addedge(a, b, d);
    }
    for (int i=1 ; i<=md ; i++) {
        scanf("%d%d%d", &a, &b, &d);
        addedge(b, a, -d);
    }
    bool flag = spfa(1);
    if (!flag) printf("-1\n");
    else if (dist[n] == INF) printf("-2\n");
    else printf("%lld\n", dist[n]);
    return 0;
}