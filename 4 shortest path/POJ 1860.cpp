#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

const int MAXE = 20001;
const int MAXV = 101;
const int INF = 0x3f3f3f3f;

struct edge{
	int v, next;
	double r, c;
} g[MAXE];

int N, M, S; double V;
int top = 1;
int temp[MAXV], head[MAXV];
double dist[MAXV];

void addedge(int u, int v, double r, double c) {
	// printf("u=%d, v=%d, r=%.1lf, c=%.1lf\n", u, v, r, c);
	g[top].v = v;
	g[top].r = r;
	g[top].c = c;
	g[top].next = head[u];
	head[u] = top++;
}

bool bellman_ford(int s, double v) {
	memset(dist, 0, sizeof(dist));
	dist[s] = v;
	for (int i=1 ; i<=N-1 ; i++) {
		for (int j=1 ; j<=N ; j++) {
			for (int k=head[j] ; k!=0 ; k = g[k].next) {
				if (dist[g[k].v] < (dist[j] - g[k].c) * g[k].r)
					dist[g[k].v] = (dist[j] - g[k].c) * g[k].r;
			}
		}
	}
	for (int j=1 ; j<=N ; j++) {
		for (int k=head[j] ; k!=0 ; k = g[k].next) {
			if (dist[g[k].v] < (dist[j] - g[k].c) * g[k].r) 
				return true;
		}
	}
	return false;
}

int main()
{
	int u, v; double rab, cab, rba, cba;
	scanf("%d%d%d%lf", &N, &M, &S, &V);
	for (int i=1 ; i<=M ; i++) {
		scanf("%d%d%lf%lf%lf%lf", &u, &v, &rab, &cab, &rba, &cba);
		addedge(u, v, rab, cab);
		addedge(v, u, rba, cba);
	}
	if (bellman_ford(S, V))
		printf("YES\n");
	else printf("NO\n");
	return 0;
}