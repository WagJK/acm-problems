#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int MAXE = 2000005;
const int MAXV = 2000005;
const int INF = 0x3f3f3f3f;

int p, q, T, top = 1;
int u[MAXE], v[MAXE], w[MAXE];
long long totcost[MAXV];

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV];
long long dist[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(visited, false, 	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
	for (int i=1 ; i<=p ; i++)
		totcost[i] += dist[i];
}

int main() {
	scanf("%d", &T);
	long long res = 0;
	for (int t=1 ; t<=T ; t++) {
		memset(totcost, 0, sizeof(totcost));
		scanf("%d%d", &p, &q);
		
		memset(head, 0, sizeof(head)); top = 1;
		for (int i=1 ; i<=q ; i++) {
			scanf("%d%d%d", &u[i], &v[i], &w[i]);
			addedge(u[i], v[i], w[i]);
		}
		dijkstra(1);
		
		memset(head, 0, sizeof(head)); top = 1;
		for (int i=1 ; i<=q ; i++)
			addedge(v[i], u[i], w[i]);
		dijkstra(1);
		
		res = 0;
		for (int i=2 ; i<=p ; i++)
			res += totcost[i];
		printf("%lld\n", res);
	}
	return 0;
}