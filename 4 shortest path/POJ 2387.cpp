#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int MAXE = 100086;
const int MAXV = 100086;
const int INF = 0x7fffffff;
const int NAN = -1;

struct edge{
	int v, w, next;
} g[MAXE];

int N, T, u, v, w, top = 1;
int dist[MAXV], head[MAXV], prev[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(prev, 	NAN, 	sizeof(prev));
	memset(visited, false, 	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] >= dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}

int main(){
	scanf("%d%d", &T, &N);
	for (int i=1 ; i<=T ; i++) {
		scanf("%d%d%d", &u, &v, &w);
		addedge(u, v, w);
		addedge(v, u, w);
	}
	dijkstra(N);
	printf("%d\n", dist[1]);
	return 0;
}