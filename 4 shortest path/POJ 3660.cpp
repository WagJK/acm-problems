#include <cstdio>
#include <iostream>

using namespace std;

const int INF = 0x3f3f3f3f;

int f[105][105];

int main()
{
    int n, m, a, b;
    scanf("%d%d", &n, &m);
    for (int i=1 ; i<=n ; i++){
        f[i][i] = 0;
        for (int j=1 ; j<=n ; j++)
            f[i][j] = INF;
    }
    for (int i=1 ; i<=m ; i++) {
        scanf("%d%d", &a, &b);
        f[a][b] = 1;
    }
    for (int k=1 ; k<=n ; k++) {
        for (int i=1 ; i<=n ; i++) {
            for (int j=1 ; j<=n ; j++)
                if (f[i][j] > f[i][k] + f[k][j])
                    f[i][j] = f[i][k] + f[k][j];
        }
    }
    int res = 0;
    for (int i=1 ; i<=n ; i++){
        int cnt = 0;
        for (int j=1 ; j<=n ; j++)
            if (i != j && (f[i][j] != INF || f[j][i] != INF)) cnt++;
        if (cnt == n-1) res++;
    }
    printf("%d\n", res);

    return 0;
}