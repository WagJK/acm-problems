#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

const int MAXE = 20001;
const int MAXV = 101;
const int INF = 0x3f3f3f3f;

int n, m, top = 1;

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV], dist[MAXV], prev[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(prev, 	0, 		sizeof(prev));
	memset(visited, false, 	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}

int main()
{
	scanf("%d", &n);
	char word[10]; int tmp;
	for (int i=2 ; i<=n ; i++) {
		for (int j=1 ; j<=i-1 ; j++) {
			scanf("%s", &word);
			if (word[0] == 'x') {
				addedge(i, j, INF);
				addedge(j, i, INF);
			} else {
				tmp = 0;
				for (int k=0 ; k<strlen(word) ; k++)
					tmp = tmp * 10 + word[k] - '0';
				addedge(i, j, tmp);
				addedge(j, i, tmp);
			}
		}
	}
	
	dijkstra(1);
	int ans = 0;
	for (int i=2 ; i<=n ; i++) {
		if (dist[i] < INF && dist[i] > ans)
			ans = dist[i];
	}
	printf("%d", ans);
	return 0;
}