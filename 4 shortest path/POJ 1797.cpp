#include <cstdio>
#include <iostream>
#include <queue>
#include <cmath>
#include <cstring>

using namespace std;

const int MAXV = 1005;
const int MAXE = 2000050;
const int INF = 0x3f3f3f3f;

int n, m, t, top;
int u[MAXE], v[MAXE], w[MAXE];

struct edge{ int v, w, next; } g[MAXE];
int dist[MAXV], head[MAXV], prev[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	
	memset(dist,	0x3f,	sizeof(dist));
	memset(prev,	0,		sizeof(prev));
	memset(visited, false,	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}


bool check(int a) {
	top = 1;
	memset(head, 0, sizeof(head));
	for (int i=1 ; i<=m ; i++) {
		if (w[i] >= a) {
			addedge(u[i], v[i], 1);
			addedge(v[i], u[i], 1);
		}
	}
	dijkstra(1);
	if (dist[n] < INF) return true;
	else return false;
}

int main() {
	scanf("%d", &t);
	for (int i=1 ; i<=t ; i++) {
		scanf("%d%d", &n, &m);
		for (int j=1 ; j<=m ; j++) {
			scanf("%d%d%d", &u[j], &v[j], &w[j]);
		}
		int ll = 0, rr = 1000000, mid, ans = 0;
		while (ll <= rr) {
			mid = (ll + rr) / 2;
			if (check(mid)) {
				ans = mid;
				ll = mid + 1;
			} else rr = mid - 1;
		}
		printf("Scenario #%d:\n", i);
		printf("%d\n\n", ans);
	}
	return 0;
}