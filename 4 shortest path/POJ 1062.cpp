#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
#include <algorithm>

using namespace std;

const int MAXE = 20050;
const int MAXV = 150;
const long long INF = 0x3f3f3f3f3f3f3f3f;

struct edge{
	int v, w, next;
} g[MAXE];

int head[MAXV];
long long dist[MAXV];
bool visited[MAXV];

int N, M;
int P[MAXV], L[MAXV], X[MAXV];
int T[MAXV][MAXV], V[MAXV][MAXV];
int level[MAXV], top = 1;

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<long long, vector<pair<long long, int> >, greater<pair<long long, int> > > q;
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(visited, false, 	sizeof(visited));
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}

int main()
{
    long long res = INF;
    scanf("%d%d", &M, &N);
    for (int i=1 ; i<=N ; i++) {
        scanf("%d%d%d", &P[i], &L[i], &X[i]);
        level[i] = L[i];
        for (int j=1 ; j<=X[i] ; j++) {
            scanf("%d%d", &T[i][j], &V[i][j]);
        }
    }
    sort(level + 1, level + N + 1);
    for (int k=1 ; k<=N ; k++) {
        int ll = level[k], rr = ll;
        for (int i=k ; i<=N ; i++) {
            if (level[i] - level[k] > M) break;
            rr = level[i];
        }
        if (L[1] < ll || L[1] > rr) continue;
        top = 1; memset(head, 0, sizeof(head));
        for (int i=1 ; i<=N ; i++) {
            if (L[i] < ll || L[i] > rr) continue;
            if (i == 1) addedge(1, N+1, P[i]);
            else        addedge(1, i, P[i]);
        }
        for (int i=1 ; i<=N ; i++) {
            for (int j=1 ; j<=X[i] ; j++) {
                if (L[i] < ll || L[i] > rr) continue;
                if (L[T[i][j]] < ll || L[T[i][j]] > rr) continue;
                // Following condition is a crucial line!
                if (T[i][j] == 1) continue;
                if (i == 1) addedge(T[i][j], N+1, V[i][j]);
                else        addedge(T[i][j], i, V[i][j]);
            }
        }
        dijkstra(1);
        if (dist[N+1] < res) res = dist[N+1];
    }
    printf("%lld\n", res);
    return 0;
}