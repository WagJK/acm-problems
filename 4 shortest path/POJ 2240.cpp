#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

const int MAXE = 2000;
const int MAXV = 50;
const int MAXLEN = 300;
const int INF = 0x3f3f3f3f;

struct edge{
	int v, next;
	double r;
} g[MAXE];

int n, m, top = 1;
char name[MAXV][MAXLEN];
int head[MAXV];
double dist[MAXV];

void addedge(int u, int v, double r) {
	g[top].v = v;
	g[top].r = r;
	g[top].next = head[u];
	head[u] = top++;
}

bool bellman_ford(int s) {
	memset(dist, 0, sizeof(dist));
	dist[s] = 1;
	for (int i=1 ; i<=n-1 ; i++) {
		for (int j=1 ; j<=n ; j++) {
			for (int k=head[j] ; k!=0 ; k = g[k].next) {
				if (dist[g[k].v] < dist[j] * g[k].r)
					dist[g[k].v] = dist[j] * g[k].r;
			}
		}
	}
	for (int j=1 ; j<=n ; j++) {
		for (int k=head[j] ; k!=0 ; k = g[k].next)
			if (dist[g[k].v] < dist[j] * g[k].r) return true;
	}
	return false;
}

int find(char* x) {
	for (int i=1 ; i<=n ; i++)
		if (strcmp(x, name[i]) == 0) return i;
	return 0;
}

int main()
{
	for (int t=1 ; true ; t++) {
		memset(head, 0, sizeof(head)); top = 1;
		scanf("%d", &n);
		if (n == 0) break;
		for (int i=1 ; i<=n ; i++)
			scanf("%s", &name[i]);
		scanf("%d", &m);
		
		char c1[MAXLEN], c2[MAXLEN]; double r;
		for (int i=1 ; i<=m ; i++) {
			scanf("%s%lf%s", &c1, &r, &c2);
			addedge(find(c1), find(c2), r);
		}
		bool res = false;
		for (int i=1 ; i<=n ; i++)
			res = res || bellman_ford(i);
		if (res) printf("Case %d: Yes\n", t);
		else printf("Case %d: No\n", t);
	}
	return 0;
}