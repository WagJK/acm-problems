#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int MAXE = 5201;
const int MAXV = 501;
const int INF = 0x3f3f3f3f;

int F, N, M, W, S, E, T;
int top = 1, totdist[MAXV];

struct edge{
	int v, w, next;
} g[MAXE];
int dist[MAXV], head[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

bool bellman_ford(int s) {
	memset(dist, 0x3f, sizeof(dist));
	dist[s] = 0;
	for (int i = 1 ; i <= N-1 ; i++) {
		for (int j = 1 ; j <= N ; j++) {
			for (int k = head[j] ; k != 0 ; k = g[k].next) {
				if (dist[g[k].v] > dist[j] + g[k].w)
					dist[g[k].v] = dist[j] + g[k].w;
			}
		}
	}
	for (int j = 1 ; j <= N ; j++) {
		for (int k = head[j] ; k != 0 ; k = g[k].next) {
			if (dist[g[k].v] > dist[j] + g[k].w) return true;
		}
	}
	return false;
}

int main()
{
	scanf("%d", &F);
	for (int t = 0 ; t < F ; t++) {
		top = 1; memset(head, 0, sizeof(head));
		
		scanf("%d%d%d", &N, &M, &W);
		for (int i = 1 ; i <= M ; i++) {
			scanf("%d%d%d", &S, &E, &T);
			addedge(S, E, T);
			addedge(E, S, T);
		}
		for (int i = 1 ; i <= W ; i++) {
			scanf("%d%d%d", &S, &E, &T);
			addedge(S, E, -T);
		}
		bool res = false;
		res = res || bellman_ford(1);
		if (res) printf("YES\n");
		else printf("NO\n");
	}
	return 0;
}