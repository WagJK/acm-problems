#include <cstdio>
#include <iostream>

using namespace std;

const int INF = 0x3f3f3f3f;

int N, A, B, K;
int f[200][200];

int main()
{
    scanf("%d%d%d", &N, &A, &B);

    for (int i=1 ; i<=N ; i++){
        for (int j=1 ; j<=N ; j++) {
            if (i == j) f[i][j] = 0;
            else f[i][j] = INF;
        }
    }
    int x;
    for (int i=1 ; i<=N ; i++) {
        scanf("%d", &K);
        for (int j=1 ; j<=K ; j++) {
            scanf("%d", &x);
            if (j == 1) f[i][x] = 0;
            else f[i][x] = 1;
        }
    }
    for (int k=1 ; k<=N ; k++) {
        for (int i=1 ; i<=N ; i++) {
            for (int j=1 ; j<=N ; j++)
                if (f[i][j] > f[i][k] + f[k][j])
                    f[i][j] = f[i][k] + f[k][j];
        }
    }
    if (f[A][B] < INF) printf("%d\n", f[A][B]);
    else printf("-1\n");

    return 0;
}