#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>

using namespace std;

const int MAXV = 605;

// spfa
int dist[MAXV];
int viscnt[MAXV];
bool inqueue[MAXV];
// main
int n, top = 1;
int c[MAXV][MAXV];

bool spfa(int s) {
    queue<int> q;
    memset(dist, 	0x3f, 	sizeof(dist));
    memset(viscnt,  0,      sizeof(viscnt));
	memset(inqueue, false, 	sizeof(inqueue));

	for (int i=1 ; i<=n ; i++) {
		if (i != s) {
			dist[i] = c[s][i];
			q.push(i); inqueue[i] = true;
		}
	}
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int v=1 ; v<=n ; v++) {
			if (dist[v] > dist[u] + c[u][v]) {
				dist[v] = dist[u] + c[u][v];
                if (!inqueue[v]) {
                    q.push(v); inqueue[v] = true;
                    if (++viscnt[v] > n) return false;
                }
			}
		}
		inqueue[u] = false;
	}
    return true;
}

int main()
{
	int res, loop1, loopn;

	while (scanf("%d", &n) != EOF) {
		for (int i=1 ; i<=n ; i++)
			for (int j=1 ; j<=n ; j++)
				scanf("%d", &c[i][j]);
		spfa(1);
		res = dist[n];
		loop1 = dist[1];
		spfa(n);
		loopn = dist[n];
		printf("%d\n", min(res, loop1 + loopn));
	}
	return 0;
}
