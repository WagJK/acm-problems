#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

const int MAXE = 400005;
const int MAXV = 100005;
const int INF = 0x3f3f3f3f;

int n, m, top = 1;

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV], dist[MAXV], prev[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(prev, 	0, 		sizeof(prev));
	memset(visited, false, 	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}
int main(){
	scanf("%d%d", &n, &m);
	int a, b, c, res = 0; top = 1;
	memset(head, 0, sizeof(head));
	for (int i=1 ; i<=m ; i++) {
		scanf("%d%d%d", &a, &b, &c);
		addedge(a, b, c);
	}
	dijkstra(1); if (dist[n] > res) res = dist[n];
	// dijkstra(n); if (dist[1] > res) res = dist[1];
	printf("%d\n", res);
	return 0;
}