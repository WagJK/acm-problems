#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>

using namespace std;

const int MAXE = 2400000;
const int MAXV = 400000;
const long long INF = 0x3f3f3f3f3f3f3f3f;

struct edge{
	int v, w, next;
} g[MAXE];

int head[MAXV];
long long dist[MAXV];
bool visited[MAXV];

int T, N, M, C;
int l[MAXV], top = 1;

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<long long, vector<pair<long long, int> >, greater<pair<long long, int> > > q;
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(visited, false, 	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}

int main()
{
    int u, v, w;
    scanf("%d", &T);
    for (int t=1 ; t<=T ; t++) {
        top = 1; memset(head, 0, sizeof(head));

        scanf("%d%d%d", &N, &M, &C);
        for (int i=1 ; i<=N ; i++){
            scanf("%d", &l[i]);
            addedge(i, N + l[i], 0);
            addedge(N + N + l[i], i, 0);
        }
        for (int i=1 ; i<=M ; i++) {
            scanf("%d%d%d", &u, &v, &w);
            addedge(u, v, w);
            addedge(v, u, w);
        }
        for (int i=1 ; i<N ; i++) {
            addedge(N + i, N + N + i + 1, C);
            addedge(N + i + 1, N + N + i, C);
        }
        dijkstra(1);
        if (dist[N] < INF) printf("Case #%d: %lld\n", t, dist[N]);
        else printf("Case #%d: -1\n", t);
    }
    return 0;
}