#include <cstdio>
#include <iostream>
#include <cstring>
#include <cfloat>
#include <queue>
#include <cmath>

using namespace std;

const long double INF = LDBL_MAX / 2;
const int MAXV = 1000;
const int MAXE = 100000;

struct edge{
	int v, next;
    long double w;
} g[MAXE];
int head[MAXV], prev[MAXV];
long double dist[MAXV];
bool visited[MAXV];

int top = 1, cnt = 0;
int xx[MAXV], yy[MAXV];

void addedge(int u, int v, long double w) {
    g[top].v = v;
    g[top].w = w;
    g[top].next = head[u];
    head[u] = top++;
}

long double d(int a, int b) {
    return sqrt((long double)(
                (xx[a] - xx[b]) * (xx[a] - xx[b]) + 
                (yy[a] - yy[b]) * (yy[a] - yy[b])));
}

void dijkstra(int s) {
    priority_queue<long double, vector<pair<long double, int> >, greater<pair<long double, int> > > q;
	
	memset(prev, 	0, 		sizeof(prev));
	memset(visited, false, 	sizeof(visited));
    for (int i=1 ; i<=cnt ; i++) dist[i] = INF;

	dist[s] = 0.0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}

int main()
{
    int sx, sy, tx, ty, x, y, px = -1, py = -1;

    scanf("%d%d%d%d", &sx, &sy, &tx, &ty);
    cnt++; xx[cnt] = sx, yy[cnt] = sy;
    cnt++; xx[cnt] = tx, yy[cnt] = ty;
    
    while(scanf("%d%d", &x, &y) != EOF) {
        if (x == -1 && y == -1) {
            px = -1, py = -1;
            continue;
        }
        cnt++; xx[cnt] = x, yy[cnt] = y;
        if (px == -1 && py == -1)
            px = x, py = y;
        else {
            addedge(cnt-1, cnt, d(cnt-1, cnt) * 15.0);
            addedge(cnt, cnt-1, d(cnt-1, cnt) * 15.0);
        }
    }

    for (int i=1 ; i<=cnt ; i++){
        for (int j=1 ; j<=cnt ; j++){
            if (i != j) {
                addedge(i, j, d(i, j)  * 60.0);
                addedge(j, i, d(i, j)  * 60.0);
            }
        }
    }
    dijkstra(1);
    printf("%d\n", int(dist[2] / 10000.0 + 0.5));

    return 0;
}