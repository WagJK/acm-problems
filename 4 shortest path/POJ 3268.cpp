#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int MAXE = 200001;
const int MAXV = 2001;
const int INF = 0x3f3f3f3f;

int n, m, x, top = 1;
int u[MAXE], v[MAXE], w[MAXE];
int totdist[MAXV];

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV], dist[MAXV], prev[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(prev, 	0, 		sizeof(prev));
	memset(visited, false, 	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
	for (int i=1 ; i<=n ; i++)
		totdist[i] += dist[i];
}

int main() {
	while(scanf("%d%d%d", &n, &m, &x) != EOF) {
		int res = 0;
		memset(totdist, 0, sizeof(totdist));
		for (int i=1 ; i<=m ; i++) 
			scanf("%d%d%d", &u[i], &v[i], &w[i]);
		
		top = 1, memset(head, 0, sizeof(head));
		for (int i=1 ; i<=m ; i++) 
			addedge(u[i], v[i], w[i]);
		dijkstra(x);
		
		top = 1, memset(head, 0, sizeof(head));
		for (int i=1 ; i<=m ; i++) 
			addedge(v[i], u[i], w[i]);
		dijkstra(x);
		
		for (int i=1 ; i<=n ; i++) {
			if (i != x && totdist[i] > res)
				res = totdist[i];
		}
		printf("%d\n", res);
	}
	return 0;
}