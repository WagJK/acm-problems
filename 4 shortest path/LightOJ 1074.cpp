#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>
#include <cmath>

using namespace std;

const int MAXE = 50000;
const int MAXV = 205;
const int INF = 0x3f3f3f3f;

int n, m, q, top = 1, T;
int b[MAXV];

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV];

int dist[MAXV], inqueue[MAXV], viscnt[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dfs(int x) {
	visited[x] = true;
	for (int k = head[x] ; k != 0 ; k = g[k].next) {
		if (!visited[g[k].v]) dfs(g[k].v);
	}
}

bool spfa(int s) {
    queue<int> q;
    memset(dist, 	0x3f, 	sizeof(dist));
    memset(viscnt,  0,      sizeof(viscnt));
	memset(inqueue, false, 	sizeof(inqueue));

	dist[s] = 0;
    q.push(s); inqueue[s] = true;
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (visited[g[i].v]) continue;
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
                if (!inqueue[g[i].v]) {
                    q.push(g[i].v); inqueue[g[i].v] = true;
                    if (++viscnt[g[i].v] > n-1) dfs(g[i].v);
                }
			}
		}
        inqueue[u] = false;
	}
    return true;
}

int main()
{
	int u, v, x, dest;
	scanf("%d", &T);
	for (int t=1 ; t<=T ; t++) {
		top = 1;
		memset(head, 0, sizeof(head));
		memset(visited, false, sizeof(visited));

		scanf("%d", &n);
		for (int i=1 ; i<=n ; i++)
			scanf("%d", &b[i]);
		scanf("%d", &m);
		for (int i=1 ; i<=m ; i++) {
			scanf("%d%d", &u, &v);
			addedge(u, v, pow(b[v] - b[u], 3));
		}
		spfa(1);
		scanf("%d", &q);
		printf("Case %d:\n", t);
		for (int i=1 ; i<=q ; i++) {
			scanf("%d", &dest);
			if (dist[dest] < INF && dist[dest] >= 3 && !visited[dest])
				printf("%d\n", dist[dest]);
			else printf("?\n");
		}
	}
	return 0;
}
