#include <cstdio>
#include <iostream>
#include <queue>
#include <cmath>
#include <cstring>

using namespace std;

const int MAXV = 201;
const int MAXE = 40001;
const int INF = 0x3f3f3f3f;

int n, t = 0, top = 1;
int x[MAXV], y[MAXV];

struct edge{ int v, w, next; } g[MAXE];
int dist[MAXV], head[MAXV], prev[MAXV];
bool visited[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	
	memset(dist,	0x3f,	sizeof(dist));
	memset(prev,	-1,		sizeof(prev));
	memset(visited, false,	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] >= dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}


bool check(int a) {
	top = 1;
	memset(head, 0, sizeof(head));
	for (int i=1 ; i<=n ; i++) {
		for (int j=i+1 ; j<=n ; j++) {
			int dist = (x[i] - x[j]) * (x[i] - x[j]) + 
					   (y[i] - y[j]) * (y[i] - y[j]);
			if (dist <= a) {
				addedge(i, j, 1);
				addedge(j, i, 1);
			}
		}
	}
	dijkstra(1);
	if (dist[2] < INF) return true;
	else return false;
}

int main() {
	while (true) {
		scanf("%d", &n);
		if (n == 0) break;
		for (int i=1 ; i<=n ; i++) {
			scanf("%d%d", &x[i], &y[i]);
		}
		int ll = 1, rr = 2000000, mid, ans;
		while (ll <= rr) {
			mid = (ll + rr) / 2;
			if (check(mid)) {
				ans = mid;
				rr = mid - 1;
			} else ll = mid + 1;
		}
		printf("Scenario #%d\n", ++t);
		printf("Frog Distance = %.3f\n\n", sqrt(ans));
	}
	return 0;
}