#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 505;
const int MAXE = 200005;

// ====== graph
int N, S, T;
struct edge{
	int v, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];

void addedge(int u, int v, int c) {
	// printf("uvc %d %d %d\n", u, v, c);
	edge e1 = {v, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

bool bfs(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i = head[u] ; i!=0 ; i = g[i].next) {
            if (level[g[i].v] < 0 && g[i].c > g[i].f) {
                level[g[i].v] = level[u] + 1;
                q.push(g[i].v);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int dfs(int u, int t, int low) {
    if (u == t || low == 0) return low;
    for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
        if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
            int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
            if (lowest != 0) {
                g[i].f += lowest, g[rev(i)].f -= lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
    }
    return flow;
}

void read(char x) {
	char temp;
	do {
		scanf("%c", &temp);
	} while (temp != x);
}

int main()
{
	int n, m, u, v, c, np, nc;
	while (scanf("%d%d%d%d", &n, &np, &nc, &m) != EOF) {
		top = 1; memset(head, 0, sizeof(head));
		S = n + 1; T = S + 1; N = T;
		for (int i=1 ; i<=m ; i++) {
			read('('); scanf("%d", &u);
			read(','); scanf("%d", &v);
			read(')'); scanf("%d", &c);
			addedge(u + 1, v + 1, c);
		}
		for (int i=1 ; i<=np ; i++) {
			read('('); scanf("%d", &u);
			read(')'); scanf("%d", &c);
			addedge(S, u + 1, c);
		}
		for (int i=1 ; i<=nc ; i++) {
			read('('); scanf("%d", &u);
			read(')'); scanf("%d", &c);
			addedge(u + 1, T, c);
		}
		printf("%d\n", dinic(S, T));
	}
	return 0;
}
