#include <cstdio>
#include <cstring>
#include <cmath>
#include <queue>
#include <vector>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 205;
const int MAXE = 30005;

int N, S, T;
// graph
struct edge{
	int v, w, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// spfa
int dist[MAXV], prev[MAXV], viscnt[MAXV];
bool inqueue[MAXV];
// main
char map[205][205];

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}
int abs(int x) {
	if (x < 0) return -x; else return x;
}
int mht_d(pair<int, int> x, pair<int, int> y) {
	return abs(x.first - y.first) + abs(x.second - y.second);
}

void addedge(int u, int v, int w, int c) {
	edge e1 = {v, w, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, -w, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

bool spfa(int s, int t) {
    queue<int> q;
    memset(dist, 	0x3f, 	sizeof(dist));
	memset(prev,	0,		sizeof(prev));
    memset(viscnt,  0,      sizeof(viscnt));
	memset(inqueue, false, 	sizeof(inqueue));

	dist[s] = 0;
    q.push(s); inqueue[s] = true; viscnt[s]++;
	while (!q.empty()) {
		int u = q.front(); q.pop(); inqueue[u] = false;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w && g[i].c > g[i].f) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = i;
                if (!inqueue[g[i].v]) {
                    q.push(g[i].v); inqueue[g[i].v] = true;
                    if (++viscnt[g[i].v] > N) return false;
                }
			}
		}
	}
	return (dist[t] != INF);
}

void mincost_maxflow(int s, int t, int &cost, int &flow) {
	cost = 0, flow = 0;
	while (spfa(s, t)) {
		int minf = INF;
		for (int i=prev[t] ; i!=0 ; i=prev[g[rev(i)].v]) {
			minf = min(minf, g[i].c - g[i].f);
		}
		for (int i=prev[t] ; i!=0 ; i=prev[g[rev(i)].v]) {
			g[i].f += minf, g[rev(i)].f -= minf;
			cost += g[i].w * minf;
		}
		flow += minf;
	}
}

int main()
{
	int r, c, n = 0, flow = 0, cost = 0;
	vector< pair<int, int> > man, home;
	while (scanf("%d%d", &r, &c) != EOF) {
		if (r == 0 || c == 0) break;
		n = flow = cost = 0;
		man.clear(); home.clear();
		top = 1; memset(head, 0, sizeof(head));

		for (int i=1 ; i<=r ; i++) {
			scanf("%s", &map[i]);
			for (int j=0 ; j<c ; j++) {
				if (map[i][j] == 'm') {
					n++; man.push_back(make_pair(i, j));
				}
				if (map[i][j] == 'H')
					home.push_back(make_pair(i, j));
			}
		}
		S = 2 * n + 1; T = S + 1; N = T;
		for (int i=0 ; i<man.size() ; i++)
			addedge(S, i+1, 0, 1);
		for (int i=0 ; i<home.size() ; i++)
			addedge(i+n+1, T, 0, 1);
		for (int i=0 ; i<man.size() ; i++) {
			for (int j=0 ; j<home.size() ; j++)
				addedge(i+1, j+n+1, mht_d(man[i], home[j]), 1);
		}
		mincost_maxflow(S, T, cost, flow);
		printf("%d\n", cost);
	}
	return 0;
}
