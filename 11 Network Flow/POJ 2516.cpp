#include <cstdio>
#include <cstring>
#include <cmath>
#include <queue>
#include <vector>

#define SHOE_KEEPER 0
#define SUPPLY 1

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 205;
const int MAXN = 105;
const int MAXE = 30005;

int N, S, T;
// graph
struct edge{
	int v, w, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// spfa
int dist[MAXV], prev[MAXV], viscnt[MAXV];
bool inqueue[MAXV];
// main
int flow, cost, n, m, k;
int totflow, totcost, totneed;
int need[MAXN][MAXN], strg[MAXN][MAXN];
int c[MAXN][MAXN][MAXN];

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

void addedge(int u, int v, int w, int c) {
	edge e1 = {v, w, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, -w, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

bool spfa(int s, int t) {
    queue<int> q;
    memset(dist, 	0x3f, 	sizeof(dist));
	memset(prev,	0,		sizeof(prev));
    memset(viscnt,  0,      sizeof(viscnt));
	memset(inqueue, false, 	sizeof(inqueue));

	dist[s] = 0;
    q.push(s); inqueue[s] = true; viscnt[s]++;
	while (!q.empty()) {
		int u = q.front(); q.pop(); inqueue[u] = false;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w && g[i].c > g[i].f) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = i;
                if (!inqueue[g[i].v]) {
                    q.push(g[i].v); inqueue[g[i].v] = true;
                    if (++viscnt[g[i].v] > N) return false;
                }
			}
		}
	}
	return (dist[t] != INF);
}

void mincost_maxflow(int s, int t, int &cost, int &flow) {
	cost = 0, flow = 0;
	while (spfa(s, t)) {
		int minf = INF;
		for (int i=prev[t] ; i!=0 ; i=prev[g[rev(i)].v]) {
			minf = min(minf, g[i].c - g[i].f);
		}
		for (int i=prev[t] ; i!=0 ; i=prev[g[rev(i)].v]) {
			g[i].f += minf, g[rev(i)].f -= minf;
			cost += g[i].w * minf;
		}
		flow += minf;
	}
}

int h(int x, int type) {
	if (type == SHOE_KEEPER) return x;
	else return n + x;
}

int main()
{
	while (scanf("%d%d%d", &n, &m, &k) != EOF) {
		if (n == 0 && m == 0 && k == 0) break;
		totneed = 0, totflow = 0, totcost = 0;
		for (int i=1 ; i<=n ; i++) {
			for (int j=1 ; j<=k ; j++) {
				scanf("%d", &need[i][j]);
				totneed += need[i][j];
			}
		}
		for (int i=1 ; i<=m ; i++)
			for (int j=1 ; j<=k ; j++)
				scanf("%d", &strg[i][j]);
		for (int t=1 ; t<=k ; t++)
			for (int i=1 ; i<=n ; i++)
				for (int j=1 ; j<=m ; j++)
					scanf("%d", &c[t][i][j]);

		for (int t=1 ; t<=k ; t++) {
			S = n + m + 1; T = S + 1; N = T;
			top = 1; memset(head, 0, sizeof(head));
			for (int i=1 ; i<=n ; i++)
				addedge(h(i, SHOE_KEEPER), T, 0, need[i][t]);
			for (int i=1 ; i<=m ; i++)
				addedge(S, h(i, SUPPLY), 0, strg[i][t]);
			for (int i=1 ; i<=n ; i++)
				for (int j=1 ; j<=m ; j++)
					addedge(h(j, SUPPLY), h(i, SHOE_KEEPER), c[t][i][j], INF);
			mincost_maxflow(S, T, cost, flow);
			totcost += cost; totflow += flow;
		}
		if (totflow == totneed) printf("%d\n", totcost);
		else printf("-1\n");
	}
	return 0;
}
