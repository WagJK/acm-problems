#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 500;
const int MAXE = 250000;

// ====== graph
int N, S, T;
struct edge{
	int v, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];
// ====== main
bool f[MAXV];

void addedge(int u, int v, int c) {
	edge e1 = {v, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, c, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

bool bfs(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i = head[u] ; i!=0 ; i = g[i].next) {
            if (level[g[i].v] < 0 && g[i].c > g[i].f) {
                level[g[i].v] = level[u] + 1;
                q.push(g[i].v);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int dfs(int u, int t, int low) {
    if (u == t || low == 0) return low;
    for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
        if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
            int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
            if (lowest != 0) {
                g[i].f += lowest, g[rev(i)].f -= lowest;
                return lowest;
            }
        }
    }
    return 0;
}

void dfs2(int u) {
	f[u] = true;
	for (int i = head[u] ; i!=0 ; i = g[i].next) {
		if (!f[g[i].v] && g[i].c > g[i].f)
			dfs2(g[i].v);
	}
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
    }
    return flow;
}

int main()
{
	int n, m, u, v, c;
	while (scanf("%d%d", &n, &m) != EOF) {
		if (n == 0 && m == 0) break;
		top = 1; memset(head, 0, sizeof(head));
		S = 1; T = 2; N = n;
		for (int i=1 ; i<=m ; i++) {
			scanf("%d%d%d", &u, &v, &c);
			addedge(u, v, c);
		}
		dinic(S, T);
		memset(f, false, sizeof(f));
		f[S] = true; dfs2(S);

		for (int i=1 ; i<top ; i++) {
			if ((i & 1) && (f[g[i].v] || f[g[rev(i)].v]) && !(f[g[i].v] && f[g[rev(i)].v]))
				printf("%d %d\n", g[i].v, g[rev(i)].v);
		}
		printf("\n");
	}
	return 0;
}
