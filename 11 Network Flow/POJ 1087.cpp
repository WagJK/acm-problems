#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 805;
const int MAXLEN = 25;

int N, S, T;
int level[MAXV];
int f[MAXV][MAXV];

char input[MAXV][MAXLEN], output[MAXV][MAXLEN], name[MAXV][MAXLEN];
int cnt[MAXV];

void addedge(int u, int v, int w) {
	// printf("uvw %d %d %d\n", u, v, w);
	f[u][v] = w;
}

bool BFS(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i=1 ; i<=N ; i++) {
            if (level[i] < 0 && f[u][i] > 0) {
                level[i] = level[u] + 1;
                q.push(i);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int find(int x, int t, int low) {
    if (x == t) return low;
    for (int i=1 ; i<=N ; i++) {
        if (f[x][i] > 0 && level[i] == level[x] + 1) {
            int lowest = find(i, t, min(low, f[x][i]));
            if (lowest != 0) {
                f[x][i] -= lowest;
                f[i][x] += lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (BFS(s, t)) {
        do {
            temp = find(s, t, INF);
            flow += temp;
        } while (temp != 0);
    }
    return flow;
}

int main()
{
	int n, m, k;
	scanf("%d", &n);
	for (int i=1 ; i<=n ; i++)
		scanf("%s", &input[i]);
	scanf("%d", &m);
	for (int i=1 ; i<=m ; i++)
		scanf("%s%s", &name[i+n], &output[i+n]);
	scanf("%d", &k);
	for (int i=1 ; i<=k ; i++)
		scanf("%s%s", &input[i+n+m], &output[i+n+m]);

	// receptacles
	char temp[MAXLEN];
	for (int i=1 ; i<=n ; i++) {
		for (int j=1 ; j<=n ; j++) {
			if (strcmp(input[i], input[j]) < 0){
				strcpy(temp, input[i]);
				strcpy(input[i], input[j]);
				strcpy(input[j], temp);
			}
		}
	}
	memset(cnt, 0, sizeof(cnt));
	int now = 1;
	for (int i=1 ; i<=n ; i++) {
		if (i == 1 || strcmp(input[i], input[i-1]) != 0)
			now = i, cnt[now] = 1;
		else if (strcmp(input[i], input[i-1]) == 0)
			cnt[now]++;
	}

	S = n + m + k + 1; T = S + 1; N = T;
	memset(f, 0, sizeof(f));
	for (int i=1 ; i<=n ; i++) {
		if (cnt[i] > 0)
			addedge(i, T, cnt[i]);
	}
	// devices
	for (int i=1 ; i<=m ; i++){
		addedge(S, i+n, 1);
		for (int j=1 ; j<=n ; j++) {
			if (cnt[i] > 0 && strcmp(output[i+n], input[j]) == 0)
				addedge(i+n, j, 1);
		}
		for (int j=1 ; j<=k ; j++) {
			if (strcmp(output[i+n], input[j+n+m]) == 0)
				addedge(i+n, j+n+m, 1);
		}
	}
	// adapters
	for (int i=1 ; i<=k ; i++) {
		for (int j=1 ; j<=n ; j++) {
			if (cnt[j] > 0 && strcmp(output[i+n+m], input[j]) == 0)
				addedge(i+n+m, j, INF);
		}
		for (int j=1 ; j<=k ; j++) {
			if (i != j && strcmp(output[i+n+m], input[j+n+m]) == 0)
				addedge(i+n+m, j+n+m, INF);
		}
	}

	printf("%d\n", m - dinic(S, T));
	return 0;
}
