#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 500;
const int MAXE = 250000;

// ====== graph
int N, S, T;
struct edge{
	int v, w, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];

void addedge(int u, int v, int c) {
	edge e1 = {v, INF, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, INF, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

bool bfs(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i = head[u] ; i!=0 ; i = g[i].next) {
            if (level[g[i].v] < 0 && g[i].c > g[i].f) {
                level[g[i].v] = level[u] + 1;
                q.push(g[i].v);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int dfs(int u, int t, int low) {
    if (u == t || low == 0) return low;
    for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
        if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
            int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
            if (lowest != 0) {
                g[i].f += lowest, g[rev(i)].f -= lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
    }
    return flow;
}

int main()
{
	int nc, nf, nd, cnf, cnd, cf, cd;
	top = 1; memset(head, 0, sizeof(head));

    scanf("%d%d%d", &nc, &nf, &nd);
    S = 2*nc + nf + nd + 1; T = S + 1; N = T;
    for (int i=1 ; i<=nc ; i++) {
        addedge(i, i + nc, 1);
        scanf("%d%d", &cnf, &cnd);
        for (int j=1 ; j<=cnf ; j++) {
            scanf("%d", &cf);
            addedge(cf + 2*nc, i, 1);
        }
        for (int j=1 ; j<=cnd ; j++) {
            scanf("%d", &cd);
            addedge(i + nc, cd + 2*nc + nf, 1);
        }
    }
    for (int i=1 ; i<=nf ; i++)
        addedge(S, i + 2*nc, 1);
    for (int i=1 ; i<=nd ; i++)
        addedge(i + 2*nc + nf, T, 1);

    printf("%d\n", dinic(S, T));
    return 0;
}
