#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 100005;
const int MAXE = 400005;

// ====== graph
int N, S, T;
struct edge{
	int v, c, f, next;
} g[MAXE];
int top = 2, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];

inline void addedge(int u, int v, int c) {
	edge e1 = {v, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, c, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

inline bool bfs(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i = head[u] ; i!=0 ; i = g[i].next) {
            if (level[g[i].v] < 0 && g[i].c > g[i].f) {
                level[g[i].v] = level[u] + 1;
                q.push(g[i].v);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

inline int dfs(int u, int t, int low) {
    if (u == t || low == 0) return low;
    for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
        if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
            int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
            if (lowest != 0) {
                g[i].f += lowest, g[i ^ 1].f -= lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
    }
    return flow;
}

int main()
{
	int caseNum, n, m, x, y, u, v, c;
	int xmin, xmax, mini, maxi;
	scanf("%d", &caseNum);
	for (int cn=1 ; cn<=caseNum ; cn++) {
		top = 2; memset(head, 0, sizeof(head));
		xmin = INF, xmax = -INF;
		scanf("%d%d", &n, &m);
		for (int i=1 ; i<=n ; i++) {
			scanf("%d%d", &x, &y);
			if (x < xmin) { xmin = x; mini = i; }
			if (x > xmax) { xmax = x; maxi = i; }
		}
		for (int i=1 ; i<=m ; i++) {
			scanf("%d%d%d", &u, &v, &c);
			addedge(u, v, c);
		}
		S = mini; T = maxi; N = n;
		printf("%d\n", dinic(S, T));
	}
	return 0;
}
