#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 405;
const int MAXE = 1000005;

// ====== graph
int N, S, T;
struct edge{
	int v, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];
// ====== main
int n, m, nf;
int a[MAXE], b[MAXE], f[MAXV];
bool tar[MAXV][MAXV];

int father(int x) {
	if (x == f[x]) return x;
	f[x] = father(f[x]);
	return f[x];
}

void addedge(int u, int v, int c) {
	edge e1 = {v, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

bool bfs(int s, int t) {
	queue<int> q;
	memset(level, 0xff, sizeof(level));
	level[s] = 0; q.push(s);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (level[g[i].v] < 0 && g[i].c > g[i].f) {
				level[g[i].v] = level[u] + 1;
				q.push(g[i].v);
			}
		}
	}
	if (level[t] > 0) return true;
	else return false;
}

int dfs(int u, int t, int low) {
	if (u == t || low == 0) return low;
	for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
		if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
			int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
			if (lowest != 0) {
				g[i].f += lowest, g[rev(i)].f -= lowest;
				return lowest;
			}
		}
	}
	return 0;
}

int dinic(int s, int t) {
	int flow = 0, temp;
	while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
	}
	return flow;
}

bool check(int x) {
	S = 2 * n + 1; T = S + 1; N = T;
	top = 1; memset(head, 0, sizeof(head));
	for (int i=1 ; i<=n ; i++) {
		addedge(i+n, T, x);
		addedge(S, i, x);
	}
	for (int i=1 ; i<=n ; i++) {
		for (int j=1 ; j<=n ; j++)
			if (tar[father(i)][j]) addedge(i, j + n, 1);
	}
	if (dinic(S, T) == x * n) return true;
	else return false;
}

int main()
{
	int caseNum, c, d, ll, rr, mid, ans;
	scanf("%d", &caseNum);
	for (int cn=1 ; cn<=caseNum ; cn++) {
		scanf("%d%d%d", &n, &m, &nf);
		for (int i=1 ; i<=n ; i++) {
			f[i] = i;
			memset(tar[i], false, sizeof(tar[i]));
		}
		for (int i=1 ; i<=m ; i++)
			scanf("%d%d", &a[i], &b[i]);
		for (int i=1 ; i<=nf ; i++) {
			scanf("%d%d", &c, &d);
			f[father(c)] = father(d);
		}
		for (int i=1 ; i<=m ; i++)
			tar[father(a[i])][b[i]] = true;
		ll = 1, rr = 200, mid, ans = 0;
		while (ll <= rr) {
			mid = (ll + rr) / 2;
			if (check(mid)) {
				ans = mid;
				ll = mid + 1;
			} else rr = mid - 1;
		}
		printf("%d\n", ans);
	}
	return 0;
}
