#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 5005;
const int MAXE = 250005;

// ====== graph
int N, S, T;
struct edge{
	int v, w, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];
// ====== main
int f[MAXV];

void addedge(int u, int v, int c) {
	edge e1 = {v, INF, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, INF, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

bool bfs(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i = head[u] ; i!=0 ; i = g[i].next) {
            if (level[g[i].v] < 0 && g[i].c > g[i].f) {
                level[g[i].v] = level[u] + 1;
                q.push(g[i].v);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int dfs(int u, int t, int low) {
    if (u == t || low == 0) return low;
    for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
        if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
            int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
            if (lowest != 0) {
                g[i].f += lowest, g[rev(i)].f -= lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
    }
    return flow;
}

int main()
{
	int n, m, select, suit, cap, res;
	while (scanf("%d%d", &n, &m) != EOF) {
		memset(f, 0, sizeof(f));
		top = 1; memset(head, 0, sizeof(head));
		for (int i=1 ; i<=n ; i++) {
			select = 0;
			for (int j=1 ; j<=m ; j++) {
				scanf("%d", &suit);
				select = (select << 1) + suit;
			}
			f[select]++;
		}
		S = (1 << m) + m + 1; T = S + 1; N = T;
		for (int i=1 ; i<=m ; i++) {
			scanf("%d", &cap);
			addedge(i + (1 << m), T, cap);
		}
		for (int i=0 ; i<(1<<m) ; i++) {
			if (f[i] == 0) continue;
			addedge(S, i, f[i]);
			int temp = i;
			for (int j=m ; j>=1 ; j--) {
				if ((temp & 1) == 1)
					addedge(i, j + (1<<m), f[i]);
				temp = temp >> 1;
			}
		}
		res = dinic(S, T);
		if (res == n) printf("YES\n");
		else printf("NO\n");
	}

	return 0;
}
