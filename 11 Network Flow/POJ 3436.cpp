#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 105;
const int MAXP = 15;

int level[MAXV];
int f[MAXV][MAXV], ff[MAXV][MAXV];
int N, S, T;

int p, n;
int q[MAXV], s[MAXV][MAXP], d[MAXV][MAXP];

void addedge(int u, int v, int w) {
	// printf("uvw %d %d %d\n", u, v, w);
	f[u][v] = w;
}

bool BFS(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i=1 ; i<=N ; i++) {
            if (level[i] < 0 && f[u][i] > 0) {
                level[i] = level[u] + 1;
                q.push(i);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int find(int x, int t, int low) {
    if (x == t) return low;
    for (int i=1 ; i<=N ; i++) {
        if (f[x][i] > 0 && level[i] == level[x] + 1) {
            int lowest = find(i, t, min(low, f[x][i]));
            if (lowest != 0) {
                f[x][i] -= lowest;
                f[i][x] += lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (BFS(s, t)) {
        do {
            temp = find(s, t, INF);
            flow += temp;
        } while (temp != 0);
    }
    return flow;
}

bool fit(int prev, int next) {
	for (int i=1 ; i<=p ; i++)
		if (s[next][i] != 2 && d[prev][i] != s[next][i]) return false;
	return true;
}
bool neednoparts(int x) {
	for (int i=1 ; i<=p ; i++)
		if (s[x][i] == 1) return false;
	return true;
}
bool haveallparts(int x) {
	for (int i=1 ; i<=p ; i++)
		if (d[x][i] == 0) return false;
	return true;
}

int main()
{
	scanf("%d%d", &p, &n);
	S = 2*n + 1, T = 2*n + 2, N = 2*n + 2;
	for (int i=1 ; i<=n ; i++) {
		scanf("%d", &q[i]);
		for (int j=1 ; j<=p ; j++)
			scanf("%d", &s[i][j]);
		for (int j=1 ; j<=p ; j++)
			scanf("%d", &d[i][j]);
	}
	for (int i=1 ; i<=n ; i++) {
		if (neednoparts(i))
			addedge(S, i, INF);
		addedge(i, n+i, q[i]);
		for (int j=1 ; j<=n ; j++) {
			if (fit(i, j)) addedge(n+i, j, INF);
		}
		if (haveallparts(i))
			addedge(n+i, T, INF);
	}
	for (int i=1 ; i<=N ; i++)
		for (int j=1 ; j<=N ; j++)
			ff[i][j] = f[i][j];

	int res = dinic(S, T), cnt = 0;
	for (int i=1 ; i<=n ; i++) {
		for (int j=1 ; j<=n ; j++)
			if (i != j && f[i][n+j] > 0) cnt++;
	}
	printf("%d %d\n", res, cnt);
	for (int i=1 ; i<=n ; i++) {
		for (int j=1 ; j<=n ; j++)
			if (i != j && f[i][n+j] > 0)
				printf("%d %d %d\n", j, i, f[i][n+j]);
	}
	return 0;
}
