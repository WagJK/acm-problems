#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 1000;
const int MAXE = 1000000;

// ====== graph
int N, S, T;
struct edge{
	int v, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];
// ====== main
int n, m, d;
char lim[100][100], liz[100][100];

void addedge(int u, int v, int c) {
	// printf("uvc %d %d %d\n", u, v, c);
	edge e1 = {v, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

bool bfs(int s, int t) {
	queue<int> q;
	memset(level, 0xff, sizeof(level));
	level[s] = 0; q.push(s);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (level[g[i].v] < 0 && g[i].c > g[i].f) {
				level[g[i].v] = level[u] + 1;
				q.push(g[i].v);
			}
		}
	}
	if (level[t] > 0) return true;
	else return false;
}

int dfs(int u, int t, int low) {
	if (u == t || low == 0) return low;
	for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
		if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
			int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
			if (lowest != 0) {
				g[i].f += lowest, g[rev(i)].f -= lowest;
				return lowest;
			}
		}
	}
	return 0;
}

int dinic(int s, int t) {
	int flow = 0, temp;
	while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
	}
	return flow;
}

int main()
{
	int caseNum, res = 0, cnt = 0;
	scanf("%d", &caseNum);
	for (int cn=1 ; cn<=caseNum ; cn++) {
		top = 1; memset(head, 0, sizeof(head));
		res = 0; cnt = 0;

		scanf("%d%d", &n, &d);
		for (int i=0 ; i<n ; i++) {
			scanf("%s", lim[i]);
			m = strlen(lim[i]);
			for (int j=0 ; j<m ; j++)
				addedge(i * m + j + 1, (n+i) * m + j + 1, lim[i][j] - '0');
		}
		for (int i=0 ; i<n ; i++) {
			for (int j=0 ; j<m ; j++) {
				for (int i2=0 ; i2<n ; i2++) {
					for (int j2=0 ; j2<m ; j2++) {
						if (i == i2 && j == j2) continue;
						if ((i - i2) * (i - i2) + (j - j2) * (j - j2) <= d * d)
							addedge((n+i) * m + j + 1, i2 * m + j2 + 1, INF);
					}
				}
			}
		}
		S = 2 * n * m + 1; T = S + 1; N = T;
		for (int i=0 ; i<n ; i++) {
			scanf("%s", liz[i]);
			for (int j=0 ; j<m ; j++) {
				if (liz[i][j] == 'L') {
					addedge(S, i * m + j + 1, 1); cnt++;
				}
				if (i < d || i > n - d - 1 || j < d || j > m - d - 1) {
					addedge((n+i) * m + j + 1, T, lim[i][j] - '0');
				}
			}
		}
		res = dinic(S, T);
		if (cnt - res == 0)
			printf("Case #%d: no lizard was left behind.\n", cn);
		else if (cnt - res == 1)
			printf("Case #%d: 1 lizard was left behind.\n", cn);
		else
			printf("Case #%d: %d lizards were left behind.\n", cn, cnt - res);
	}
	return 0;
}
