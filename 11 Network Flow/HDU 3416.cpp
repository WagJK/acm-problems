#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 2005;
const int MAXE = 400005;

// ====== graph
int N, S, T;
struct edge{
	int v, w, c, f, next;
} g[MAXE];
int top = 1, head[MAXV];
// ====== dinic
int level[MAXV], curr[MAXV];
// ====== dijkstra
int dist[MAXV];
bool visited[MAXV];
// ====== main
int caseNum, n, m, s, t;
int a[MAXE], b[MAXE], c[MAXE];
int dist1[MAXV], dist2[MAXV];

void addedge_d(int u, int v, int w) {
	edge e = {v, w, INF, INF, head[u]};
	g[top] = e; head[u] = top++;
}

void addedge_f(int u, int v, int c) {
	edge e1 = {v, INF, c, 0, head[u]};
	g[top] = e1; head[u] = top++;
	edge e2 = {u, INF, 0, 0, head[v]};
	g[top] = e2; head[v] = top++;
}

int rev(int x) {
	if (x % 2 == 0) return x - 1; else return x + 1;
}

bool bfs(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i = head[u] ; i!=0 ; i = g[i].next) {
            if (level[g[i].v] < 0 && g[i].c > g[i].f) {
                level[g[i].v] = level[u] + 1;
                q.push(g[i].v);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int dfs(int u, int t, int low) {
    if (u == t || low == 0) return low;
    for (int &i = curr[u] ; i!=0 ; i = g[i].next) {
        if (g[i].c > g[i].f && level[g[i].v] == level[u] + 1) {
            int lowest = dfs(g[i].v, t, min(low, g[i].c - g[i].f));
            if (lowest != 0) {
                g[i].f += lowest, g[rev(i)].f -= lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (bfs(s, t)) {
		memcpy(curr, head, sizeof(curr));
		while (temp = dfs(s, t, INF)) flow += temp;
    }
    return flow;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(visited, false, 	sizeof(visited));
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}

int main()
{
	scanf("%d", &caseNum);
	for (int cn=1 ; cn<=caseNum ; cn++) {
		scanf("%d%d", &n, &m);
		for (int i=1 ; i<=m ; i++)
			scanf("%d%d%d", &a[i], &b[i], &c[i]);
		scanf("%d%d", &s, &t);

		top = 1; memset(head, 0, sizeof(head));
		for (int i=1 ; i<=m ; i++) addedge_d(a[i], b[i], c[i]);
		dijkstra(s); memcpy(dist1, dist, sizeof(dist));

		top = 1; memset(head, 0, sizeof(head));
		for (int i=1 ; i<=m ; i++) addedge_d(b[i], a[i], c[i]);
		dijkstra(t); memcpy(dist2, dist, sizeof(dist));

		top = 1; memset(head, 0, sizeof(head));
		for (int i=1 ; i<=m ; i++) {
			if (dist1[a[i]] + dist2[b[i]] + c[i] == dist1[t])
				addedge_f(a[i], b[i], 1);
		}
		printf("%d\n", dinic(s, t));
	}
	return 0;
}
