#include <cstdio>
#include <iostream>

using namespace std;

const int INF = 2147483647;

int g[20][20], resg[20][20], now[20][20];
int cnt, m, n;

void flip(int x, int y) {
    cnt++;
    now[x][y] = 1 - now[x][y];
    if (x+1 <= m) now[x+1][y] = 1 - now[x+1][y];
    if (y+1 <= n) now[x][y+1] = 1 - now[x][y+1];
    if (x-1 >= 1) now[x-1][y] = 1 - now[x-1][y];
    if (y-1 >= 1) now[x][y-1] = 1 - now[x][y-1];
}

int main()
{
    scanf("%d%d", &m, &n);
    for (int i=1 ; i<=m ; i++) {
        for (int j=1 ; j<=n ; j++)
            scanf("%d", &g[i][j]);
    }

    int res = INF, resm = INF;
    for (int k=0 ; k<(1<<n) ; k++) {
        cnt = 0;
        for (int i=1 ; i<=m ; i++) {
            for (int j=1 ; j<=n ; j++)
                now[i][j] = g[i][j];
        }
        int temp = k;

        for (int i=1 ; i<=n ; i++) {
            if (temp & 1) flip(1, n-i+1);
            temp = temp >> 1;
        }
        for (int i=2 ; i<=m ; i++) {
            for (int j=1 ; j<=n ; j++ )
                if (now[i-1][j] == 1) flip(i, j);
        }
        bool flag = true;
        for (int i=1 ; i<=n ; i++) {
            if (now[m][i] == 1) flag = false;
        }
        if (flag && cnt < res)
            res = cnt, resm = k;
    }
    if (res == INF) printf("IMPOSSIBLE\n");
    else {
        int temp = resm;
        for (int i=1 ; i<=m ; i++) {
            for (int j=1 ; j<=n ; j++)
                now[i][j] = g[i][j];
        }
        for (int i=1 ; i<=n ; i++) {
            if (temp & 1) {
                resg[1][n-i+1] = 1;
                flip(1, n-i+1);
            }
            else resg[1][n-i+1] = 0;
            temp = temp >> 1;
        }
        for (int i=2 ; i<=m ; i++) {
            for (int j=1 ; j<=n ; j++ ) {
                if (now[i-1][j] == 1) {
                    resg[i][j] = 1;
                    flip(i, j);
                }
                else resg[i][j] = 0;
            }
        }
        for (int i=1 ; i<=m ; i++){
            for (int j=1 ; j<=n ; j++)
                printf("%d ", resg[i][j]);
            printf("\n");
        }
    }
    return 0;
}