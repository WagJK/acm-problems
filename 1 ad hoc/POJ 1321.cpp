#include <cstdio>
#include <iostream>
using namespace std;

int n, k;
int res = 0;
char map[10][10];

bool table[10][10];
bool check(int i, int j) {
	if (map[i][j] == '.') return false;
	for (int k=1 ; k<=n ; k++){
		if (k != i && table[k][j]) return false;
		if (k != j && table[i][k]) return false;
	}
	return true;
}

void dfs(int step) {
	if (step == k) {
		res++; return;
	}
	int lasti = 1, lastj = 0;
	for (int i=1 ; i<=n ; i++) {
		for (int j=1 ; j<=n ; j++)
			if (table[i][j])
				lasti = i, lastj = j;
	}
	for (int i=lasti ; i<=n ; i++) {
		for (int j=1 ; j<=n ; j++) {
			if (i == lasti && j <= lastj) continue;
			if (check(i, j)) {
				table[i][j] = true;
				dfs(step + 1);
				table[i][j] = false;
			}
		}
	}
}

int main()
{
	char garbage;
	while(true) {
		scanf("%d%d%c", &n, &k, &garbage);
		if (n == -1 && k == -1) break;
		for (int i=1 ; i<=n ; i++) {
			for (int j=1 ; j<=n ; j++)
				scanf("%c", &map[i][j]);
			scanf("%c", &garbage);
		}
		for (int i=1 ; i<=n ; i++) {
			for (int j=1 ; j<=n ; j++)
				table[i][j] = false;
		}
		res = 0;
		dfs(0);
		printf("%d\n", res);
	}
	return 0;
}