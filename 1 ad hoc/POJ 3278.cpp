#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

const int MAXE = 600000;
const int MAXV = 200000;
const int INF = 0x3f3f3f3f;

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV], dist[MAXV], prev[MAXV];
bool visited[MAXV];

int n, k, top = 1;

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<int, vector<pair<int, int> >, greater<pair<int, int> > > q;
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(prev, 	0, 		sizeof(prev));
	memset(visited, false, 	sizeof(visited));
	
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				prev[g[i].v] = u;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}

int main(){
    scanf("%d%d", &n, &k);
    for (int i=0 ; i<200000 ; i++){
        addedge(i, i+1, 1);
        addedge(i+1, i, 1);
    }
    for (int i=1 ; i<=100000 ; i++)
        addedge(i, 2*i, 1);
    dijkstra(n);
    printf("%d", dist[k]);
    return 0;
}