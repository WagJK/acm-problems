#include <cstdio>
#include <iostream>
#include <queue>

using namespace std;

struct pos{ int x, y, z, t; };

int L, R, C;
char dungeon[40][40][40];
bool visited[40][40][40];
queue<pos> q; 

int main() {
	while (scanf("%d%d%d", &L, &R, &C) != EOF)
	{
		pos s, e;
		if (L == 0 && R == 0 && C == 0) break;
		for (int i=1 ; i<=L ; i++) {
			for (int j=1 ; j<=R ; j++) {
				scanf("%s", &dungeon[i][j]);
				for (int k=0 ; k<C ; k++) {
					visited[i][j][k] = false;
					if (dungeon[i][j][k] == 'S') {
						s.x = i, s.y = j, s.z = k, s.t = 0;
						visited[i][j][k] = true;
					}
					if (dungeon[i][j][k] == 'E')
						e.x = i, e.y = j, e.z = k;
				}
			}
		}
		int res = -1;
		while (!q.empty()) q.pop(); q.push(s);
		while (!q.empty()) {
			// printf("%d %d %d t = %d\n", q.front().x, q.front().y, q.front().z, q.front().t);
			if (q.front().x == e.x && q.front().y == e.y && q.front().z == e.z) {
				res = q.front().t; break;
			} else {
				if (q.front().x + 1 <= L && dungeon[q.front().x + 1][q.front().y][q.front().z] != '#' 
				&& !visited[q.front().x + 1][q.front().y][q.front().z]){
					pos next = {q.front().x + 1, q.front().y, q.front().z, q.front().t + 1};
					visited[q.front().x + 1][q.front().y][q.front().z] = true;
					q.push(next);
				}
				if (q.front().x - 1 >= 1 && dungeon[q.front().x - 1][q.front().y][q.front().z] != '#' 
				&& !visited[q.front().x - 1][q.front().y][q.front().z]){
					pos next = {q.front().x - 1, q.front().y, q.front().z, q.front().t + 1};
					visited[q.front().x - 1][q.front().y][q.front().z] = true;
					q.push(next);
				}
				if (q.front().y + 1 <= R && dungeon[q.front().x][q.front().y + 1][q.front().z] != '#'
				&& !visited[q.front().x][q.front().y + 1][q.front().z]){
					pos next = {q.front().x, q.front().y + 1, q.front().z, q.front().t + 1};
					visited[q.front().x][q.front().y + 1][q.front().z] = true;
					q.push(next);
				}
				if (q.front().y - 1 >= 1 && dungeon[q.front().x][q.front().y - 1][q.front().z] != '#'
				&& !visited[q.front().x][q.front().y - 1][q.front().z]){
					pos next = {q.front().x, q.front().y - 1, q.front().z, q.front().t + 1};
					visited[q.front().x][q.front().y - 1][q.front().z] = true;
					q.push(next);
				}
				if (q.front().z + 1 < C && dungeon[q.front().x][q.front().y][q.front().z + 1] != '#' 
				&& !visited[q.front().x][q.front().y][q.front().z + 1]){
					pos next = {q.front().x, q.front().y, q.front().z + 1, q.front().t + 1};
					visited[q.front().x][q.front().y][q.front().z + 1] = true;
					q.push(next);
				}
				if (q.front().z - 1 >= 0 && dungeon[q.front().x][q.front().y][q.front().z - 1] != '#'
				&& !visited[q.front().x][q.front().y][q.front().z - 1]){
					pos next = {q.front().x, q.front().y, q.front().z - 1, q.front().t + 1};
					visited[q.front().x][q.front().y][q.front().z - 1] = true;
					q.push(next);
				}
			}
			
			q.pop();
		}
		if (res != -1) printf("Escaped in %d minute(s).\n", res);
		else printf("Trapped!\n");
	}
	return 0;
}