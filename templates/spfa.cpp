#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>

using namespace std;

const int MAXE = 2400000;
const int MAXV = 400000;
const long long INF = 0x3f3f3f3f3f3f3f3f;

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV]
int N, top = 1;;
// spfa
long long dist[MAXV];
int viscnt[MAXV];
bool inqueue[MAXV];

void addedge(int u, int v, int w) {
	edge e = {v, w, head[u]};
	g[top] = e; head[u] = top++;
}

bool spfa(int s) {
    queue<int> q;
    memset(dist, 	0x3f, 	sizeof(dist));
    memset(viscnt,  0,      sizeof(viscnt));
	memset(inqueue, false, 	sizeof(inqueue));
	dist[s] = 0;
    q.push(s); inqueue[s] = true;
	while (!q.empty()) {
		int u = q.front(); q.pop(); inqueue[u] = false;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
                if (!inqueue[g[i].v]) {
                    q.push(g[i].v); inqueue[g[i].v] = true;
                    if (++viscnt[g[i].v] > N) return false;
                }
			}
		}
	}
    return true;
}
