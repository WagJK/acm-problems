#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int INF = 0x3f3f3f3f;
const int MAXV = 105;
const int MAXP = 15;

int level[MAXV];
int f[MAXV][MAXV];
int N, S, T;

void addedge(int u, int v, int w) {
	f[u][v] = w;
}

bool BFS(int s, int t) {
    queue<int> q;
    memset(level, 0xff, sizeof(level));
    level[s] = 0; q.push(s);
    while (!q.empty()) {
        int u = q.front(); q.pop();
        for (int i=1 ; i<=N ; i++) {
            if (level[i] < 0 && f[u][i] > 0) {
                level[i] = level[u] + 1;
                q.push(i);
            }
        }
    }
    if (level[t] > 0) return true;
    else return false;
}

int find(int x, int t, int low) {
    if (x == t) return low;
    for (int i=1 ; i<=N ; i++) {
        if (f[x][i] > 0 && level[i] == level[x] + 1) {
            int lowest = find(i, t, min(low, f[x][i]));
            if (lowest != 0) {
                f[x][i] -= lowest;
                f[i][x] += lowest;
                return lowest;
            }
        }
    }
    return 0;
}

int dinic(int s, int t) {
    int flow = 0, temp;
    while (BFS(s, t)) {
        do {
            temp = find(s, t, INF);
            flow += temp;
        } while (temp != 0);
    }
    return flow;
}
