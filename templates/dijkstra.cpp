#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>

using namespace std;

const int MAXE = 2400000;
const int MAXV = 400000;
const long long INF = 0x3f3f3f3f3f3f3f3f;

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV];

long long dist[MAXV];
bool visited[MAXV];

int n, top = 1;

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

void dijkstra(int s) {
	priority_queue<long long, vector<pair<long long, int> >, greater<pair<long long, int> > > q;
	memset(dist, 	0x3f, 	sizeof(dist));
	memset(visited, false, 	sizeof(visited));
	dist[s] = 0;
	q.push(make_pair(dist[s], s));
	while (!q.empty()) {
		int u = q.top().second; q.pop();
		if (visited[u]) continue;
		visited[u] = true;
		for (int i = head[u] ; i!=0 ; i = g[i].next) {
			if (dist[g[i].v] > dist[u] + g[i].w) {
				dist[g[i].v] = dist[u] + g[i].w;
				q.push(make_pair(dist[g[i].v], g[i].v));
			}
		}
	}
}
