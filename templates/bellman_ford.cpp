#include <cstdio>
#include <iostream>
#include <cstring>
#include <queue>

using namespace std;

const int MAXE = 5201;
const int MAXV = 501;
const int INF = 0x3f3f3f3f;

struct edge{
	int v, w, next;
} g[MAXE];
int head[MAXV];

int dist[MAXV];

void addedge(int u, int v, int w) {
	g[top].v = v;
	g[top].w = w;
	g[top].next = head[u];
	head[u] = top++;
}

bool bellman_ford(int s) {
	memset(dist, 0x3f, sizeof(dist));
	dist[s] = 0;
	for (int i = 1 ; i <= N-1 ; i++) {
		for (int j = 1 ; j <= N ; j++) {
			for (int k = head[j] ; k != 0 ; k = g[k].next) {
				if (dist[g[k].v] > dist[j] + g[k].w)
					dist[g[k].v] = dist[j] + g[k].w;
			}
		}
	}
	for (int j = 1 ; j <= N ; j++) {
		for (int k = head[j] ; k != 0 ; k = g[k].next) {
			if (dist[g[k].v] > dist[j] + g[k].w) return true;
		}
	}
	return false;
}